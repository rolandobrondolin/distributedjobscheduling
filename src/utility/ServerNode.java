package utility;

import java.net.Inet4Address;
import java.rmi.Naming;

import rmi.Node;
import setup.Setup;


public class ServerNode {
	public static void main(String args[]) {

		String ip, name;
		
		name = args[0];
		
		try 
		{ 
			ip = Inet4Address.getLocalHost().getHostAddress();
			Node n = new Node(name, ip);
			Naming.rebind(n.getNodeId(), n);
			n.startUp();
			
			while(true){
				Thread.sleep(Setup.getRefreshConsoleTime());
				//System.out.print("\033[H\033[2J");
				System.out.println(n.printNodeStatus());
			}
		} 
		catch (Exception e) 
		{ 
			System.out.println("Error Node "+ name); 
			e.printStackTrace(); 
		} 
	}
}
