package utility;

import java.net.Inet4Address;
import java.rmi.Naming;
import rmi.Node;
import setup.Setup;


public class Node1 {
	public static void main(String args[]) {

		try 
		{ 
			String ip = Inet4Address.getLocalHost().getHostAddress();
			Node n = new Node("Server1", ip);
			Naming.rebind(n.getNodeId(), n);
			n.startUp();
			
		
			while(true){
				Thread.sleep(Setup.getRefreshConsoleTime());
				System.out.println(n.printNodeStatus());
			}
		} 
		catch (Exception e) 
		{ 
			System.out.println("Error Node 1"); 
			e.printStackTrace(); 
		} 
	}
}
