package executor;

import job.IJob;
import queue.IPermanentQueue;

public class Executor extends Thread {
	private IPermanentQueue jobs;
	
	public Executor(IPermanentQueue j){
		this.jobs=j;
	}
	
	@Override
	public void run(){
		IJob j;
		while(true){
			j=jobs.getNextJob();
			if(j != null){
				j.getWork().run();
				jobs.finalizeJob(j.getId(), j.getWork().getResult());
			}
		}
	}
	
}
