package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import job.ParametricRunnable;
import job.Job;
import job.Result;
import rmi.INode;


public class Controller {
	
	 public static void main(String arg[]) { 
		 INode node;
		 System.out.println("Server address :    (//127.0.0.1:1099/ServerName)");	
		 BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
	     String server;
		try {
			server = bufferRead.readLine();
			node=(INode) Naming.lookup(server);
	  	
		} catch (IOException e1) {
			System.out.println("Input Error");
			return;
		} catch (NotBoundException e) {
			System.out.println("Can't reach this server");
			return;
		}
		 
	    String cmd="";
		do{
			System.out.println("Command option : ");
			System.out.println("s :  submit a job ");
			System.out.println("r :  retrive a job ");
			System.out.println("q :  exit ");
			try {			
				cmd = bufferRead.readLine();
				if(cmd.equals("s")==false && cmd.equals("r")==false && cmd.equals("q")==false){
					System.out.println("Command not valid");
					continue;
				}
			} catch (IOException e1) {
				System.out.println("Command not valid");
				continue;	
			}
			
			switch(cmd){
				case "s":
							String id;
							try {
								id = node.submitJob(new Job(new ParametricRunnable(null)));
								if(id!=null){
									System.out.println("Submited job : "+id);
								}
								else{
									System.out.println("Job refused! Please retry!");
								}
								
							} catch (RemoteException e) {
								System.out.println("Can't reach : "+server);
							}
							break;
				case "r":
							String idJob="";
							Result r ;
							try {	
								System.out.println("Insert Job id : ");
								idJob = bufferRead.readLine();
								r = node.getJobResult(idJob);
								if(r==null){
									System.out.println("Job not computed yet");
								}else{
									System.out.println(r.print());
								}
							} catch (IOException e1) {
								System.out.println("Id job not valid");
								continue;	
							}
							break;
				case "p":
							break;
				default:
							break;
			}
		}while(!cmd.equals("q"));
		System.out.println("Controller ended");
	   } 
}
