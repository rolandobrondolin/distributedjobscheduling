package queue;

import java.util.ArrayList;
import java.util.List;

import job.IJob;
import job.Result;


// Used only for early development and testing
public class NotPermanentQueue implements IPermanentQueue {

	private List<IJob> jobs;
	private int pendingJobs;
	private int completedJobs;
	
	public NotPermanentQueue(){
		jobs= new ArrayList<IJob>();
		pendingJobs=0;
		completedJobs=0;
	}
	
	@Override
	public synchronized boolean add(IJob j) {
		for(IJob job:jobs){
			if (job.getId().equals(j.getId())){
				return false;
			}
		}
		jobs.add(j);
		//just to be sure about the fact that the job is still to be computed
		j.setStatus(false);
		pendingJobs++;
		notifyAll();
		return true;
	}


	@Override
	public synchronized IJob getJobByID(String id) {
		for(int i=0; i<jobs.size();i++){
			if (jobs.get(i).getId().equals(id)){
				return jobs.get(i);
			}
		}
		return null;
	}
	
	@Override
	public synchronized String printJobStatus() {
		StringBuffer s=new StringBuffer();
		for(IJob j:jobs){
			s.append(j.getId()+ "    " + (j.getStatus()?"completed":"pending") + "\n");
		}
		return s.toString();
	}
	
	@Override
	public synchronized IJob getNextJob() {
		while(getPendingJobsSize()==0){
			try {
				wait();
			} catch (InterruptedException e) {
				return null;
			}
		}
		for(int i=0; i<jobs.size();i++){
			if (!jobs.get(i).getStatus()){
				return jobs.get(i);
			}
		}
		return null;
	}
	
	@Override
	public synchronized boolean finalizeJob(String id , Result r) {
		for(int i=0; i<jobs.size();i++){
			if (jobs.get(i).getId().equals(id)){
				jobs.get(i).setResult(r);
				jobs.get(i).setStatus(true);
				if(pendingJobs > 0)
				{
					pendingJobs--;
					completedJobs++;
				}
				else
				{
					pendingJobs = 0;
					completedJobs++;
				}
				return true;
			}
		}
		return false;
	}

	@Override
	public synchronized int getPendingJobsSize() {
		return pendingJobs;
	}


	@Override
	public synchronized int getCompletedJobsSize() {
		return completedJobs;
	}

	@Override
	//for this queue this method is useless
	public boolean restoreQueue() {
		return false;
	}
	

}
