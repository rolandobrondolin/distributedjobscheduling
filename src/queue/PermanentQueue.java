package queue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import job.IJob;
import job.Result;
import setup.Setup;



public class PermanentQueue implements IPermanentQueue{
	String path;
	String node;
	final String fileName = Setup.getSnapshotFile();
	private List<IJob> jobs;
	
	public PermanentQueue(String node){
		path="Job";
		this.node=node;
		jobs= new CopyOnWriteArrayList<IJob>();
		File dir = new File(node);
		if (!dir.exists()){
			if (dir.mkdirs()) {
				System.out.println("Directory Job is created!");
			} else {
				System.out.println("Failed to create directory!");
			}
		}
	 }



	@Override
	public  boolean add(IJob j) {
		for(IJob i:jobs){
			if(j.getId().equals(i.getId())){
				return false;
			}
		}
		jobs.add(j);
		
		synchronized(this){
		    write(jobs);
			notifyAll();
		}
		return true;
	}

	
	
	@Override
	public IJob getJobByID(String id) {
		for(IJob i:jobs){
			if(i.getId().equals(id)){
				return i.copy(); 
			}
		}
		return null;
	}
	
	
	@Override
	public String printJobStatus() {
		StringBuffer s=new StringBuffer();
		for(IJob j:jobs){
			s.append(j.getId()+ "    " + (j.getStatus()?"completed":"pending") + "\n");
		}
		return s.toString();
	}
	
	@Override
	public IJob getNextJob() {
		synchronized(this){
			//one thread executor
			if(getPendingJobsSize()==0){
				try {
					wait();
				} catch (InterruptedException e) {
					return null;
				}
			}
		}
		for(IJob i:jobs){
			if (!i.getStatus()){
				return i.copy();
			}
		}
		return null;
	}
	
	@Override
	public boolean finalizeJob(String id , Result r) {
		for(IJob i:jobs){
			if (i.getId().equals(id)){
				i.setResult(r);
				i.setStatus(true);
				synchronized(this){
					write(jobs);
				}
				return true;
			}
		}
		return false;
	}
	
	@Override
	public synchronized int getPendingJobsSize() {
		int count=0;
		for(IJob i:jobs){
			if(!i.getStatus()){
				count++;
			}
		}
		return count;
	}


	@Override
	public synchronized int getCompletedJobsSize() {
		int count=0;
		for(IJob i:jobs){
			if(i.getStatus()){
				count++;
			}
		}
		return count;
	}

	@Override
	public synchronized boolean restoreQueue() {
		
		try{		
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(node + File.separator + fileName));
			Object obj=ois.readObject();
			if(obj instanceof List<?>){	
				jobs = (List<IJob>)obj;
			}
			ois.close();
			return true;
		}
		catch (FileNotFoundException e){
			//file not found, so nothing to restore
			//e.printStackTrace();
		} catch (IOException e) {
			//file ended, everything is ok!
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			//error during read process, this is for sure a problem!
			e.printStackTrace();
		}

		return false;
	}


	
	private void write(Object obj){
		try{
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(node + File.separator + fileName));
			oos.writeObject(obj);
			oos.flush();
			oos.close();
		}
		catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}


