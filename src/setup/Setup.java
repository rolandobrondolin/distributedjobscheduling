package setup;

public class Setup {
	private static int jobWorkTime = 10000; //10^-3 second
	private static String snapshotFile ="queue_snapshot.dat";
	private static boolean permanentQueue=true;
	private static int refreshConsoleTime =1000; //10^-3 second
	public static int jobWorkTime() {
		return jobWorkTime;
	}
	public static String getSnapshotFile() {
		return snapshotFile;
	}
	public static boolean isPermanentQueue() {
		return permanentQueue;
	}
	public static int getRefreshConsoleTime() {
		return refreshConsoleTime;
	}
}
