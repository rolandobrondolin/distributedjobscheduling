package rmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import executor.Executor;
import job.IJob;
import job.Result;
import queue.IPermanentQueue;
import queue.NotPermanentQueue;
//import queue.NotPermanentQueue;
import queue.PermanentQueue;
import setup.Setup;

public class Node extends UnicastRemoteObject implements INode {

	private static final long serialVersionUID = 1L;
	private String nodeId;
	private String nodeIp;
	private ConcurrentHashMap<String, INode> net;
	private IPermanentQueue jobs;
	private Executor exec;
	
	public Node(String id, String ip) throws RemoteException {
		super();
		this.nodeId=id;
		this.nodeIp = ip;
		this.net = new ConcurrentHashMap<String,INode>();
		
		if(Setup.isPermanentQueue()){
			this.jobs = new PermanentQueue(id);
			jobs.restoreQueue();
		}else{
			this.jobs = new NotPermanentQueue();
		}
		this.exec=new Executor(jobs);
		this.exec.start();	
	}

	public void startUp() {
		init();
		discoverNodes();
	}

	

	
	// -------------------------------------     NETWORK  ----------------------------------------------------

	@Override
	public  void connect(INode me) throws RemoteException {
		net.put(me.getFullId(), me);
	}

	@Override
	public  String getNodeId() throws RemoteException {
		return nodeId;
	}
	
	@Override
	public  String getIp() throws RemoteException {
		return nodeIp;
	}
	
	@Override
	public  String getFullId() throws RemoteException {
		return (nodeIp + "-" + nodeId);
	}

	//helper method that generates the list of all the connected nodes
	@Override
	public  ArrayList<String> getNetwork() throws RemoteException {
		ArrayList<String> buffer=new ArrayList<String>();
		for(String sNode : net.keySet()){
			if(sNode!=null){
				try {
					buffer.add("//" + net.get(sNode).getIp() + ":" + Registry.REGISTRY_PORT + "/" + net.get(sNode).getNodeId());
				} catch (RemoteException e) {
					net.remove(sNode);
				}
			}
		}
		return buffer;
	}



	//-----------------------------------------            DISPATCH    ----------------------------------------
	@Override
	public boolean pushJob(IJob j) throws RemoteException {
		if(jobs.add(j)){
			return true;
		}
		return false;
	}

	@Override
	public boolean getStatusJob(String id) throws RemoteException {
		return jobs.getJobByID(id).getStatus();
	}

	@Override
	public int loadSize() throws RemoteException {
		return jobs.getPendingJobsSize();
	}

	@Override
	public Result retriveJobResult(String id) throws RemoteException {
		IJob j=jobs.getJobByID(id);
		if(j!=null){
			return j.getResult();
		}
		return null;
	}

	@Override
	public boolean isYourJob(String id) throws RemoteException {
		if(jobs.getJobByID(id)!=null){
			return true;
		}
		return false;
	}

	// -----------------------------------------      CONTROLLER  --------------------------------------------
	@Override
	public  String submitJob(IJob job) throws  RemoteException {		
		INode dest=dispatch();					
		String id = dest.getValidId();
		job.setId(id);
		if(dest.pushJob(job)){
			return id;
		}
		return null;
	}

	@Override
	public Result getJobResult(String id) throws RemoteException {
		if(jobs.getJobByID(id)!=null){
			return jobs.getJobByID(id).getResult();
		}
		else{
			int index = id.lastIndexOf("-");
			//check index boundaries
			if(index>0){
				String key = id.substring(0,index);
				INode n = net.get(key);
				
				if(n!= null && n.isYourJob(id)){
					return n.getJobResult(id);
				}
			}
	
		}
		return null;
	}
	
	@Override
	public String getValidId() throws RemoteException {
		String idGen;
		java.util.Date date= new java.util.Date();
		idGen = this.getFullId() + "-" + (new Timestamp(date.getTime())).getTime() +genID();

		return idGen;
	}
	

// print utility method
	public String printNodeStatus(){
		StringBuffer s = new StringBuffer();
		s.append("--------------------- START Node "+ nodeId +" ---------------------\n");
		s.append("--------------------- Jobs ---------------------\n");
		s.append("Completed jobs : " + jobs.getCompletedJobsSize()+"\n");
		s.append("Pending jobs   : " + jobs.getPendingJobsSize()+"\n");
		s.append(jobs.printJobStatus()+"\n");
		s.append("--------------------- Net  ---------------------\n");
		s.append(printNetwork()+"\n");
		s.append("--------------------- END Node "+ nodeId +"   ---------------------\n");
		return s.toString();
	}
	
	private String printNetwork() {
		StringBuffer s=new StringBuffer();
		for(String sn: net.keySet()){
			if(sn!=null){
				INode n = net.get(sn);
				try {
					s.append(n.getNodeId()+"\n");
				} catch (RemoteException e) {
					net.remove(sn);
				}
			}	
		}
		return s.toString();
	}
	
	// -------------------------------------  UTILITY -------------------------------------
	
		//dispatch must take into account all the nodes to have a balanced network
		//no hierarchy, no load balancing node and no update messages leads to a complete search
		//through the graph
		private  INode dispatch(){
			INode destination = this;
			int bestLoad=jobs.getPendingJobsSize();
			int tmp_load;
			
			for(String sn:net.keySet()){
				if(sn!=null){
					INode n = net.get(sn);
					try {
						tmp_load=n.loadSize();
						if(bestLoad>tmp_load){
							destination = n;
							bestLoad=tmp_load;
							
							if(bestLoad==0){
								break;
							}
						}
					} catch (RemoteException e) {
						net.remove(sn);
					}
				}
			
				
			}
			return destination;
		}
		
		private String genID(){
			String uuid = UUID.randomUUID().toString();
			return uuid.replace("-","");

		}

	
//-------------------------------------------------------------------------------------------------------------------------------------		
		

		private  void discoverNodes(){
			HashMap<String,INode> newNodes;
			do{
				newNodes = new HashMap<>();
				for(String sn: net.keySet()){
					if(sn!=null){
						INode n = net.get(sn);
						try{
							ArrayList<String> nodes = n.getNetwork();
							for(String s: nodes){
								if(!s.endsWith(nodeIp +":"+ Registry.REGISTRY_PORT + "/" + nodeId)){
									INode node=(INode)Naming.lookup(s);
									if(!net.containsKey(node.getFullId())){
										node.connect(this);
										net.put(node.getFullId(), node);
										newNodes.put(node.getFullId(), node);
									}
								}
							}
						}
						catch (RemoteException | MalformedURLException | NotBoundException e){
							//continue with next nodes, current node is offline and we can't add it
						}
					}
				
				}	
			}while(newNodes.size()!=0);
		}
		
		
		
		private void init(){
			for(String s : Bootstrap.getList()){
				try {
					if(!s.endsWith(nodeId)){
						INode n=(INode)Naming.lookup(s);
						if(!net.containsKey(n.getFullId())){
							n.connect(this);
							net.put(n.getFullId(), n);
						}
					}
				} catch (MalformedURLException | NotBoundException | RemoteException e) {
					//System.out.println("Can't reach server : "+s);
				}
			}
		}
		
}
