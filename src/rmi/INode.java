package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import job.IJob;
import job.Result;

public interface INode extends Remote{
	//Controller
	String submitJob(IJob job) throws RemoteException;
	Result getJobResult (String id)throws RemoteException;
	String getValidId () throws RemoteException;
	
	//Dispatch
	boolean pushJob(IJob job) throws RemoteException;
	boolean isYourJob(String id) throws RemoteException ;
	boolean getStatusJob(String id) throws RemoteException;
	int loadSize()throws RemoteException;
	Result retriveJobResult(String id) throws RemoteException;
	
	//Network
	String getNodeId() throws RemoteException;
	String getIp() throws RemoteException;
	String getFullId() throws RemoteException;
	void connect(INode me) throws RemoteException;
	ArrayList<String> getNetwork() throws RemoteException;
}
