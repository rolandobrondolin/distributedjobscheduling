package job;

import java.io.Serializable;

public class Result implements Serializable {

	private static final long serialVersionUID = -465444950982068880L;
	private String result;
	
	public Result (String r){
		this.result=r;
	}
	
	public String print(){
		return result;
	}
}
