package job;

import java.io.Serializable;

public class Job implements IJob,Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3157545790220814223L;
	private String id;
	private boolean status;
	private Result res;
	private final IParametricRunnable work;
	
	public Job(IParametricRunnable w){
		this.id=null;
		this.work=w;
		this.status=false;
		res=null;
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public boolean setId(String id) {
		if(this.id==null){
			this.id = id;
			return true;
		}
		return false;
	}

	@Override
	public Result getResult() {
		return res;
	}

	@Override
	public synchronized boolean getStatus() {
		return status;
	}

	@Override
	public synchronized void setStatus(boolean s) {
		status=s;	
	}
	
	@Override
	public void setResult(Result r) {
		this.res=r;
	}

	@Override
	public IParametricRunnable getWork() {
		return work;
	}

	@Override
	public IJob copy() {
		IJob copy = new Job(work);
		copy.setId(id);
		copy.setResult(res);
		copy.setStatus(status);
		return copy;
	}

	

	

}
