package job;

import java.io.Serializable;
import java.util.ArrayList;

public interface IParametricRunnable extends Runnable,Serializable {

	public void setParameters(ArrayList<Object> par);
	public ArrayList<Object> getParameters();
	public Result getResult();
	
}
