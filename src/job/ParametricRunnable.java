package job;

import java.util.ArrayList;
import java.util.UUID;

import setup.Setup;

public class ParametricRunnable implements IParametricRunnable{


	private static final long serialVersionUID = 3704576724685893978L;
	private ArrayList<Object> parameter;
	private Result result;
	
	public ParametricRunnable(ArrayList<Object> par){
		parameter=par;
	}
	
	
	@Override
	public void run() {
		try {
			Thread.sleep(Setup.jobWorkTime());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		this.result = new Result("result-"+ genID());
		
	}
	
	@Override
	public Result getResult() {
		return result;
	}


	@Override
	public void setParameters(ArrayList<Object> par) {
		parameter=par;
	}


	@Override
	public ArrayList<Object> getParameters() {
		return parameter;
	}
	
	private String genID(){
		String uuid = UUID.randomUUID().toString();
		return uuid;

	}

}
