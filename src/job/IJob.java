package job;

import java.io.Serializable;

public interface IJob extends Serializable{
	String getId();
	boolean setId(String id);
	IParametricRunnable getWork();
	Result getResult();
	void setResult(Result r);
	boolean getStatus();
	void setStatus(boolean s);
	IJob copy();
}
