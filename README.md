# README #

This is the Distributed job scheduling project developed for the Distributed Systems class. The authors are Rolando Brondolin and Roberto Conigliaro.


In order to run the project do as follows:

* Install jdk 8
* build the project
* go to bin folder
* launch rmiregistry
* in the same folder launch servers and clients with the following commands:
* Server: 
```
#!bash

java -classpath ./ -Djava.rmi.server.hostname="host_ip" -Djava.rmi.server.codebase=file:./ utility.Node1
```

* Client: 
```
#!bash

java -classpath ./ controller.Controller
```

To deploy on multiple servers, change IP addresses of the first 4 nodes in the bootstrap list according to the host IPs, build the project on each machine and run the Servers where stated in the bootstrap list.

To run dynamic nodes:


```
#!bash

java -classpath ./ -Djava.rmi.server.hostname="host_ip" -Djava.rmi.server.codebase=file:./ utility.ServerNode "nodename"
```

### Licence ###

Copyright 2016 Roberto Conigliaro, Rolando Brondolin

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.